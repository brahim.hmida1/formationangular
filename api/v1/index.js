const express=require('express');
const blogpost = require('../models/blogpost');
const router=express.Router();
const Blogpost=require('../models/blogpost');
const multer=require('multer');
const crypto=require("crypto");
const path=require("path");
const resize=require('../../utils/resize');
const mongoose = require('mongoose');
let lastUploadedImageName='';
const storage=multer.diskStorage({
	destination:'./uploads/',
	filename:function(req,file,callback){
		crypto.pseudoRandomBytes(16,function(err,raw){
			if(err) return callback(err);
			//callback(null,raw.toString('hex')+ path.extname(file.originalname));
			lastUploadedImageName=raw.toString('hex')+path.extname(file.originalname);
			console.log('lastUploadedImageName',lastUploadedImageName);
			callback(null,lastUploadedImageName);



		})
	}
})
const upload =multer({storage:storage});

router.post('/blog-posts/images',upload.single('image'),(req,res)=>{
	if(!req.file.originalname.match(/\.(jpg|jpeg|png|gif)$/))
	{
	return	res.status(400).json({msg:"only images"});

	}
 res.status(201).send({filename:req.file.filename,file:req.file});
});


router.get('/test',(req,res)=>{
	res.status(200).json({msg:'test valide',date: new Date()});
});
router.get('/blog-posts', (req, res) => {
	blogpost.find()
		.sort({'createdOn': -1})
		.exec()
		.then(blogposts=>res.status(200).json(blogposts))
		.catch(err=>res.status(500).json({
			message:'blog posts not found',
			error:err
		}));
});
router.post('/blog-posts',(req,res)=>{
console.log('req.body', req.body);
	console.log('lastUploadedImageName', lastUploadedImageName);
	const smallImagePath = `./uploads/${lastUploadedImageName}`;
	const outputName = `./uploads/small-${lastUploadedImageName}`;
	resize({path: smallImagePath, width: 200, height: 200, outputName: outputName })
		.then(data => {
			console.log('OK resize', data.size);
		})
		.catch(err => console.error('err from resize', err));
	const blogPost = new Blogpost({
		...req.body, 
		image: lastUploadedImageName,
		smallImage: `small-${lastUploadedImageName}`
	});
	blogPost.save((err, blogPost) => {
		if (err) {
			return res.status(500).json(err);
		}
		res.status(201).json(blogPost);
	});
 
});
router.get('/blog-posts/:id', (req, res) => {
	var id=req.params.id;
	blogpost.findById(id)
		.exec()
		.then(blogposts=>res.status(200).json(blogposts))
		.catch(err=>res.status(500).json({
			message:'blog posts not found',
			error:err
		}));
});
router.put('/blog-posts/:id',upload.single('image'),(req,res)=>{
	const id=req.params.id;
	const conditions={_id:id};
	const blogPost={...req.body,image:lastUploadedImageName};
	const update={$set:blogPost};
	const options={
		upsert:true,
		new:true
	};
	Blogpost.findOneAndUpdate(conditions,update,options,(err,response)=>{
		if(err) return res.status(500).json({msg:'update failed',error:err });
		res.status(200).json({msg:`document updated successfully ${id}`,response:response});

	})


})


router.delete('/blog-posts/:id', (req, res) => {
	// retrieves the query parameter: http://localhost:3000/api/v1/blog-posts?ids=5c1133b8225e420884687048,5c1133b6225e420884687047
/*	const ids = req.query.ids;
	console.log('query allIds', ids);
	const allIds = ids.split(',').map(id => {
		// casting as a mongoose ObjectId	
		if (id.match(/^[0-9a-fA-F]{24}$/)) {
			return mongoose.Types.ObjectId((id));		 
		}else {
			console.log('id is not valid', id);
			return -1;
		}
	});
	const condition = { _id: { $in: allIds} };
	Blogpost.deleteMany(condition, (err, result) => {
		if (err) {
			return res.status(500).json(err);
		}
		res.status(202).json(result);
	});
    */
	const id=req.params.id;
	Blogpost.findByIdAndDelete(id,(err,blogpost)=>{
		if(err){
			return res.status(500).json(err);

		}
		res.status(202).json({msg:`blogpost deleted successfully ${blogpost.id}`});
	});
});
router.delete('/blog-posts', (req, res) => {
	if(!req.isAuthenticated){
		return res.status(401).json({result:'Ko',msg:'failed authenticated'});
	}
	const ids = req.query.ids;
	console.log('query allIds', ids);
	const allIds = ids.split(',').map(id => {
		// casting as a mongoose ObjectId	
		if (id.match(/^[0-9a-fA-F]{24}$/)) {
			return mongoose.Types.ObjectId((id));		 
		}else {
			console.log('id is not valid', id);
			return -1;
		}
	});
	const condition = { _id: { $in: allIds} };
	Blogpost.deleteMany(condition, (err, result) => {
		if (err) {
			return res.status(500).json(err);
		}
		res.status(202).json(result);
	});
	});


module.exports=router;