const express = require('express');



const app = express();

const uploadsDir=require('path').join(__dirname,'/uploads');
console.log('uploadsDir',uploadsDir);
app.use(express.static(uploadsDir));

const api = require('./api/v1');
const auth=require('./auth/routes');
const User=require('./auth/models/user');



const cors=require('cors');



const bodyParser=require('body-parser');







const mongoose=require('mongoose');



const connection=mongoose.connection;



app.use(bodyParser.json());



app.use(bodyParser.urlencoded({extended:false}));







app.set('port',(process.env.port || 3000));



app.use(cors({
	credentials:true,
	origin:'http://localhost:4200'
}));

// parti auth passport


const passport=require('passport');
const cookieParser=require('cookie-parser');
const session=require('express-session');
const Strategy=require('passport-local').Strategy;
app.use(cookieParser());
app.use(session({
	secret:"secretkeysecretkey",
	resave:true,
	saveUninitialized:true,
	name:'whisky-cookie'
}));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user,cb)=>{
	cb(null,user);
});

passport.deserializeUser((user,cb)=>{
	cb(null,user);
})
passport.use(new Strategy({
	usernameField:'username',
	passwordField:'password'
},(name,pwd,cb)=>{
User.findOne({username:name},(err,user)=>{
	if(err){
		console.error(`could not find ${name} in mongodb `,err);
	}
	if(user.password!==pwd){
		console.log(`password wrong for ${name}`);

	}
	else{
		console.log(`${name}  found in mongodb and authenticated`);
		cb(null,user);
	}
})
}))







app.use('/api/v1',api);
app.use('/auth',auth);



app.use((req,res)=>{



	const err=new Error('404 - not found');



	err.status=404;



	res.json(err);







});



mongoose.connect('mongodb://localhost:27017/whiskycms', { useNewUrlParser: true,useUnifiedTopology: true });



connection.on('error',(err)=>{



	console.log(`connection t mongodb establish ${err.message}`);



});



connection.once('open',()=>{



	console.log('connected to mongodb');



});



app.listen(app.get('port'),()=>{



	console.log(`express server run ${app.get('port')}`);



});