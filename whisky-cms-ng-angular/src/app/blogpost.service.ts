import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Blogpost } from './models/blogpost';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlogpostService {
baseUrl="http://localhost:3000/api/v1";
  constructor(private http: HttpClient) { }
  private blogpostcreated=new Subject<string>();
  dispatchBlogpostCreated(id:string){
    this.blogpostcreated.next(id);

  }
  uploadImage(formData:FormData){
    return this.http.post<any>(`${this.baseUrl}/blog-posts/images`,formData);

  }
  updateBlogpost(id: string, blogpost: Blogpost) {
    return this.http.put(`${this.baseUrl}/${id}`, blogpost);
  }
  handleBlogpostCreated(){
    return this.blogpostcreated.asObservable();
  }
  createBlogpost(blogpost:Blogpost){
    return this.http.post<Blogpost>(`${this.baseUrl}/blog-posts`,blogpost);
  }

  getBlogPosts():Observable<Blogpost[]>{
    return this.http.get<Blogpost[]>(`${this.baseUrl}/blog-posts`);
  }
  getBlogPostById(id: string): Observable<Blogpost> {
    return this.http.get<Blogpost>(`${this.baseUrl}/blog-posts/${id}`);
  }
  deleteSingleBlogPost(id:string) {
    return this.http.delete<Blogpost>(`${this.baseUrl}/blog-posts/${id}`);
  }
  deleteBlogposts(ids:string[]){
    const allids =ids.join(",");
    return this.http.delete(`${this.baseUrl}/blog-posts?ids=${allids}`)
  }
}
