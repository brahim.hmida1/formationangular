import { Routes } from '@angular/router';
import { AccueilComponent } from '../accueil/accueil.component';
import { AdminComponent } from '../admin/admin.component';
import { AuthComponent } from '../auth/auth.component';
import { BlogpostEditComponent } from '../blogpost-edit/blogpost-edit.component';
import { BlogpostListComponent } from '../blogpost-list/blogpost-list.component';
import { BlogpostComponent } from '../blogpost/blogpost.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
export const routes: Routes = [
  {path:'',component:AccueilComponent},
  { path: 'blog',  component: BlogpostListComponent },
  {path:'auth',component:AuthComponent},
  {path:'admin', component:AdminComponent},
  {path:'admin/blog-posts/:id',component:BlogpostEditComponent},
  { path: 'blog-posts/:id', component: BlogpostComponent },
  { path: '**',component:PageNotFoundComponent}
  ];