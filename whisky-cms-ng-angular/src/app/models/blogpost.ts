export interface Blogpost{
    _id:string;
    title:string;
    subtitle:string;
    image:string;
    smallImage:String;
    content:string;

}