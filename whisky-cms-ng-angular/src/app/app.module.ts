import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import 'hammerjs';
import { BlogpostComponent } from './blogpost/blogpost.component';
import { BlogpostListComponent } from './blogpost-list/blogpost-list.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BlogpostService } from './blogpost.service';
import { MaterialModule } from './material.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AdminComponent } from './admin/admin.component';
import { BlogpostAddComponent } from './blogpost-add/blogpost-add.component';
import { BlogpostEditComponent } from './blogpost-edit/blogpost-edit.component';
import { AccueilComponent } from './accueil/accueil.component';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import { NgxEditorModule } from 'ngx-editor';
import { AuthComponent } from './auth/auth.component';
import { AddCookieInterceptor } from './add-cookie.interceptor';
import { FlexLayoutModule } from "@angular/flex-layout";

@NgModule({
  declarations: [
    AppComponent,
    BlogpostComponent,
    BlogpostListComponent,
    PageNotFoundComponent,
    AdminComponent,
    BlogpostAddComponent,
    BlogpostEditComponent,
    AccueilComponent,
    AuthComponent,


  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    IvyCarouselModule,
    NgxEditorModule,
    FlexLayoutModule,



  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,useClass: AddCookieInterceptor ,multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
